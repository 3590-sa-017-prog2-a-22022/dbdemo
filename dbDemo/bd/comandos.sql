-- SQL: STRUCTURED QUERY LANGUAGE / LENGUAJE ESTRUCTURADO DE CONSULTA
USE dbprogra2;
TRUNCATE TABLE usuario;
SELECT * FROM usuario;

INSERT INTO usuario(usuario, contrasenia, email, telefono) 
VALUES('jcordon', '123', 'jcordon@miumg.edu.gt', '123');

INSERT INTO usuario(usuario, contrasenia, email, telefono) 
VALUES('cperez', '456', 'cperez@miumg.edu.gt', '123');

INSERT INTO usuario(usuario, contrasenia, email, telefono) 
VALUES('malvarez', '789', 'malvarez@miumg.edu.gt', '123');

INSERT INTO usuario(usuario, contrasenia, email, telefono) 
VALUES('ggonzalez', '789', 'ggonzalez@miumg.edu.gt', '123');

UPDATE usuario SET telefono = '555' WHERE idusuario = 4;
UPDATE usuario SET telefono = '999' WHERE idusuario > 0;

DELETE FROM usuario WHERE idusuario = 3;

-- CRUD: create (insert), read (select), update, delete
INSERT INTO venta(codigo_producto, precio, nit, tipo_pago) VALUES(28473858, 10, '282474-d', 'CONTADO');
INSERT INTO venta(codigo_producto, precio, nit, tipo_pago) VALUES(25, 50, '282474-d', 'CONTADO');


-- OBTENER VERSIÓN DE MYSQL
SELECT @@version version_mysql;

