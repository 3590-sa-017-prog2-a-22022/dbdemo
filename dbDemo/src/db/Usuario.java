/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package db;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;

public class Usuario 
{
    private int idusuario;
    private String usuario;
    private String contrasenia;
    private String email;
    private String telefono;
    
    public String error;
    
    private DbProgra2 dbProgra2;
    
    public Usuario(int _idusuario, String _usuario, String _contrasenia, String _email, String _telefono)
    {
        idusuario = _idusuario;
        usuario = _usuario;
        contrasenia = _contrasenia;
        email = _email;
        telefono = _telefono;
        
        dbProgra2 = new DbProgra2();
    }
    
    public boolean insertar()
    {
        boolean resultado = false;
        
        try
        {
            String insert = "" +
                    "INSERT INTO usuario(usuario, contrasenia, email, telefono) " +
                    "VALUES(?, ?, ?, ?);";
        
            dbProgra2.Open();
            
            PreparedStatement pstmt = dbProgra2.conn.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
            
            pstmt.setString(1, usuario);
            pstmt.setString(2, contrasenia);
            pstmt.setString(3, email);
            pstmt.setString(4, telefono);
            
            int rowAffected = pstmt.executeUpdate();
            if(rowAffected == 1)
            {
                ResultSet rs = pstmt.getGeneratedKeys();
                if(rs.next())
                    idusuario = rs.getInt(1);
            }
            dbProgra2.Close();
            resultado = true;
            
        } catch (Exception e) 
        {
            error = "insertar(). " + e.getMessage();
        }
        
        return resultado;
    }
    
    public int getId(){
        return idusuario;
    }
}
