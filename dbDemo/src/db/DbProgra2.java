/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package db;

import java.sql.Connection;
import java.sql.DriverManager;


public class DbProgra2 
{
    private String connectionString = "jdbc:mysql://localhost/dbprogra2?user=root&password=admin&useSSL=false";
    public Connection conn = null;
    public String msgError = "";
    
    public DbProgra2()
    {
    }
    
    public boolean Open()
    {
        boolean resultado = false;
        try 
        {
            conn = DriverManager.getConnection(connectionString);
            resultado = true;
            
        } catch (Exception e)
        {
            msgError = "Ocurrió un error en conexión de base de datos: " + e.getMessage();
        }
        
        return resultado;
    }
    
    public boolean Close()
    {
        boolean resultado = false;
        try 
        {
            if(conn != null) conn.close();
            
        } catch (Exception e)
        {
            msgError = "Ocurrió un error en conexión de base de datos: " + e.getMessage();
        }
        
        return resultado;
    }  
}
